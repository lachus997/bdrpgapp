import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna pozwalajacego na wybor id probki do transfuzji (usuniecia z tabeli jednostek)
 */
public class KontrolerOknaNowegoBohatera  {

    @FXML
    private Button Wroc;

    @FXML
    private Button nowyBohater;

    @FXML
    private TextField imieField;

    @FXML
    private TextField nazwiskoField;

    @FXML
    private TextField klasaField;

    @FXML
    private TextField rasaField;

    @FXML
    private TextField silaField;

    @FXML
    private TextField zrecznoscField;

    @FXML
    private TextField kondycjaField;

    @FXML
    private TextField inteligencjaField;

    @FXML
    private TextField madroscField;

    @FXML
    private TextField charyzmaField;

    @FXML
    private TextField wiekField;

    @FXML
    private TextField wagaField;

    @FXML
    private TextField wzrostField;

    @FXML
    private TextField poziomField;

    @FXML
    private TextField pancerzField;

    @FXML
    private MenuButton plecSplit;

    @FXML
    private MenuButton kampaniaSplit;

    @FXML
    private TextField characterField;

    @FXML
    private Label blad;

    private String plecV;

    private String login;

    private Integer kampaniaId;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * metoda wywolujaca polaczenie z baza dzieki URLowi z poprzednich kontrolerow, a nastepnie usuwajaca rejestr z tabeli jednostki na podstawie id wpisanego do textfield
     */
    @FXML
    void dodajGracza(ActionEvent event) throws SQLException {

        String imie = imieField.getText();
        String nazwisko = nazwiskoField.getText();
        String rasa = rasaField.getText();
        String klasa = klasaField.getText();
        String character = characterField.getText();
        Integer sila = Integer.parseInt(silaField.getText());
        Integer zrecznosc = Integer.parseInt(zrecznoscField.getText());
        Integer kondycja = Integer.parseInt(kondycjaField.getText());
        Integer inteligencja = Integer.parseInt(inteligencjaField.getText());
        Integer madrosc = Integer.parseInt(madroscField.getText());
        Integer charyzma = Integer.parseInt(charyzmaField.getText());
        Integer poziom = Integer.parseInt(poziomField.getText());
        Integer klasa_pancerza = Integer.parseInt(pancerzField.getText());
        Integer waga = Integer.parseInt(wagaField.getText());
        Integer wzrost = Integer.parseInt(wzrostField.getText());
        Integer wiek = Integer.parseInt(wiekField.getText());

        Integer id = null;
        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
                PreparedStatement statement;
            if(kampaniaId!=null) {
                statement = conn.prepareStatement("insert into bohater(imie,nazwisko_pseudonim,rasa,klasa,poziom,sila,zrecznosc,kondycja,inteligencja,madrosc,charyzma,plec,charakter,waga,wiek,wzrost,klasa_pancerza,gracz_id,kampania_id,stan) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,(select id from  gracz where login = ?),?,'Z') ");
                statement.setInt(19,kampaniaId);
            } else {
                statement = conn.prepareStatement("insert into bohater(imie,nazwisko_pseudonim,rasa,klasa,poziom,sila,zrecznosc,kondycja,inteligencja,madrosc,charyzma,plec,charakter,waga,wiek,wzrost,klasa_pancerza,gracz_id,stan) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,(select id from  gracz where login = ?),'Z') ");
            }
            statement.setString(1,imie);
            statement.setString(2,nazwisko);
            statement.setString(3,rasa);
            statement.setString(4,klasa);
            statement.setInt(5,poziom);
            statement.setInt(6,sila);
            statement.setInt(7,zrecznosc);
            statement.setInt(8,kondycja);
            statement.setInt(9,inteligencja);
            statement.setInt(10,madrosc);
            statement.setInt(11,charyzma);
            statement.setString(12,plecV);
            statement.setString(13,character);
            statement.setInt(14,waga);
            statement.setInt(15,wiek);
            statement.setInt(16,wzrost);
            statement.setInt(17,klasa_pancerza);
            statement.setString(18,login);

            int result = statement.executeUpdate();
            if(result!=1) {
                blad.setText("Cos poszlo nie tak");
            } else {
                blad.setText("Poprawnie");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        conn.close();
    }

    /**
     * @param event
     * @throws IOException
     * Powrot do okna pozwalajacego na wybor funkcji
     */
        @FXML
    void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuGracza.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaMenuGracza controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }


    public void start() {
            MenuItem menuItemF = new MenuItem("F");
            MenuItem menuItemM = new MenuItem("M");
            menuItemF.setOnAction(a->
                    {plecV = "F";
                    plecSplit.setText("F");
                    }
                    );
        menuItemM.setOnAction(a->
                {plecV = "M";
                plecSplit.setText("M");
                }
        );

            plecSplit.getItems().addAll(menuItemF,menuItemM);

            MenuItem menuItemZ = new MenuItem("Zadna");
            menuItemZ.setOnAction(a->{
                kampaniaId=null;
                kampaniaSplit.setText("Nie przypisana");
            });
            kampaniaSplit.getItems().add(menuItemZ);

        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
            PreparedStatement statement = conn.prepareStatement("select id,tytul from kampania where id in (select  kampania_id from gracz_kampania where gracz_id = (select id from gracz where login = ?))");
            statement.setString(1,login);

            System.out.println(login);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                 MenuItem menuItemK = new MenuItem(resultSet.getString("tytul").replace('_',' '));
                 int id = resultSet.getInt("id");
                 menuItemK.setOnAction(a->{
                     try {
                         kampaniaId = id;
                         kampaniaSplit.setText(menuItemK.getText());
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 });
                 kampaniaSplit.getItems().add(menuItemK);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
