import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

/**
 * Kontroler okna pozwalajacego na wybor id probki do transfuzji (usuniecia z tabeli jednostek)
 */
public class KontrolerOknaEdycjiKampanii  {

    @FXML
    private Button Wroc;

    @FXML
    private Button NowaKampania;

    @FXML
    private TextField tytulField;

    @FXML
    private TextField settingField;

    @FXML
    private TextField maxField;

    @FXML
    private TextField opisField;

    @FXML
    private Label blad;

    private int id;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    public void Start() throws SQLException {

        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e){
            e.printStackTrace();
        }

        try{
            PreparedStatement statement = conn.prepareStatement("select * from kampania where id = ?");
            statement.setInt(1,id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            tytulField.setText(resultSet.getString("tytul"));
            settingField.setText(resultSet.getString("setting"));
            maxField.setText(String.valueOf(resultSet.getInt("max_graczy")));
            opisField.setText(resultSet.getString("opis"));
        } catch (Exception e){
            e.printStackTrace();
            blad.setText("Cos poszlo nie tak przy ladowaniu danych");
        }
        conn.close();
    }

    /**
     * @param event
     * metoda wywolujaca polaczenie z baza dzieki URLowi z poprzednich kontrolerow, a nastepnie usuwajaca rejestr z tabeli jednostki na podstawie id wpisanego do textfield
     */
    @FXML
    void edytujKampanie(ActionEvent event) throws SQLException, IOException {

        String tytul = tytulField.getText();
        String setting = settingField.getText();
        String opis = opisField.getText();
        Integer max = null;
        try {
            max = Integer.parseInt(maxField.getText());
        } catch (Exception e){
            e.printStackTrace();
            blad.setText("Max musi byc liczba");
        }

        Connection conn = null;

        try {

            PreparedStatement statement = conn.prepareStatement("update kampania set tytul = ?, setting = ?, max_graczy = ?, opis =? where id = ?");
            statement.setString(1,tytul);
            statement.setString(2,setting);
            statement.setInt(3,max);
            statement.setString(4,opis);
            statement.setInt(5,id);

            int result = statement.executeUpdate();
            if(result!=1) {
                blad.setText("Cos poszlo nie tak");
            } else {
                blad.setText("Poprawnie");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            blad.setText("Cos poszlo nie tak");
        }
        conn.close();
        Powrot(event);
    }

    /**
     * @param event
     * @throws IOException
     * Powrot do okna pozwalajacego na wybor funkcji
     */
        @FXML
    void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuMistrza.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaMenuMistrza controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }


    public void setid(int id) {
            this.id = id;
    }

}
