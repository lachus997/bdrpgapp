import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;
import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna sluzacego do wyswietlania widoku zapasy w przygotowanej tabeli
 */
public class KontrolerOknaKampaniGracza {

    @FXML
    private Button Wroc;

    @FXML
    private TableView<KampaniaMin> tabelka;

    private ObservableList<KampaniaMin> kampaniaMins;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * @throws IOException Ponowne otworzenie okna wyboru funkcji i zamkniecie otwartego okna
     */
    @FXML
    void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuGracza.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaMenuGracza controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @throws SQLException Metoda ta pozwala na nawiazanie polaczenia, wywolacnie select all na widoku zapasy i wyswietlenie jego zawartosci w tabeli
     *                      W funkcji tej najpierw wszystkie kolumnt tabeli sa przygotowane na "odbior danych" wszystkie dane zawarte w widoku sa przetwarzane
     *                      tak, aby mozna je bylo zobrazowac w tabeli
     */
    @FXML
    public void wypelnij() throws SQLException {
        String zap = "Select * from moje_kampanie where id in (select  kampania_id from  gracz_kampania where gracz_id = (select id from gracz where login = '" + login +"' ))";
        System.out.println(zap);
        kampaniaMins = FXCollections.observableArrayList();

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            Statement statement = conn.prepareStatement(zap);
            ResultSet rs = statement.executeQuery(zap);
            System.out.println();
            System.out.println(zap);
            System.out.println();
            System.out.println();
            System.out.println(zap);
            System.out.println();

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {

                // We are using non property style for making dynamic table
                if (i > 0) {
                    String columnName = rs.getMetaData().getColumnName(i + 1);
                    TableColumn<KampaniaMin, String> col = new TableColumn<>(columnName);
                    col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, String>, ObservableValue<String>>() {
                        public ObservableValue<String> call(TableColumn.CellDataFeatures<KampaniaMin, String> param) {
                            KampaniaMin kamp = param.getValue();
                            String cellData = kamp.getValue(columnName);
                            return new SimpleStringProperty(cellData);
                        }
                    });
                    tabelka.getColumns().add(col);
                    System.out.println("Column [" + i + "] ");
                }
            }

            TableColumn<KampaniaMin, Boolean> col_action = new TableColumn<>("Szczegoly");
            col_action.setSortable(false);
            col_action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_action.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonCell2(tabelka);
                }
            });
            tabelka.getColumns().add(col_action);
//
            TableColumn<KampaniaMin, Boolean> col_Delete = new TableColumn<>("Zrezygnuj");
            col_Delete.setSortable(false);
            col_Delete.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_Delete.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonDelete(tabelka);
                }
            });
            tabelka.getColumns().add(col_Delete);

            /********************************
             * Data added to ObservableList *
             ********************************/
            while (rs.next()) {
                KampaniaMin kampaniaMin = new KampaniaMin();

                kampaniaMin.setId(rs.getInt("id"));
                kampaniaMin.setTytul(rs.getString("tytul"));
                kampaniaMin.setSetting(rs.getString("setting"));
                kampaniaMin.setProwadzacy(rs.getString("prowadzacy"));
                kampaniaMin.setLiczba_graczy(rs.getInt("liczba_graczy"));
                kampaniaMin.setMax_graczy(rs.getInt("max_graczy"));
                kampaniaMins.add(kampaniaMin);
            }
            // FINALLY ADDED TO TableView
            tabelka.setItems(kampaniaMins);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }


    private void listujGraczy(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoSzczegolowKampanii2.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaSzczegolowKampanii2 controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        controler.setid(id);
        controler.Start();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private void usunKampanie(int id) throws SQLException {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement = conn.prepareStatement("delete from gracz_kampania where gracz_id = (select id from  gracz where login = ?) and kampania_id = ?");
            statement.setString(1, login);
            statement.setInt(2,id);
            int result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement = conn.prepareStatement("update bohater set kampania_id = null where kampania_id = ?");
            statement.setInt(1,id);
            int result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        conn.close();
    }

    private class ButtonCell2 extends TableCell<KampaniaMin, Boolean> {
        final Button cellButton = new Button("Szczegoly");

        ButtonCell2(final TableView<KampaniaMin> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        listujGraczy(kampaniaMins.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    private class ButtonDelete extends TableCell<KampaniaMin, Boolean> {
        final Button delButton = new Button("Rezygnuj");

        ButtonDelete(final TableView<KampaniaMin> tblView) {
            delButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        usunKampanie(kampaniaMins.get(getIndex()).getId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    kampaniaMins.remove(getIndex());
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(delButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
}
