import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class WolneKampanie {
    private SimpleIntegerProperty id;
    private SimpleStringProperty tytul;
    private SimpleStringProperty setting;
    private SimpleStringProperty prowadzacy;
    private SimpleIntegerProperty max_graczy;
    protected SimpleIntegerProperty wolne_miejsca;

    public WolneKampanie() {
        id = new SimpleIntegerProperty();
        tytul = new SimpleStringProperty();
        setting = new SimpleStringProperty();
        prowadzacy = new SimpleStringProperty();
        max_graczy = new SimpleIntegerProperty();
        wolne_miejsca = new SimpleIntegerProperty();
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getTytul() {
        return tytul.get();
    }

    public SimpleStringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getSetting() {
        return setting.get();
    }

    public SimpleStringProperty settingProperty() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting.set(setting);
    }

    public String getProwadzacy() {
        return prowadzacy.get();
    }

    public SimpleStringProperty prowadzacyProperty() {
        return prowadzacy;
    }

    public void setProwadzacy(String prowadzacy) {
        this.prowadzacy.set(prowadzacy);
    }

    public int getMax_graczy() {
        return max_graczy.get();
    }

    public SimpleIntegerProperty max_graczyProperty() {
        return max_graczy;
    }

    public void setMax_graczy(int max_graczy) {
        this.max_graczy.set(max_graczy);
    }

    public int getWolne_miejsca() {
        return wolne_miejsca.get();
    }

    public SimpleIntegerProperty wolne_miejscaProperty() {
        return wolne_miejsca;
    }

    public void setWolne_miejsca(int wolne_miejsca) {
        this.wolne_miejsca.set(wolne_miejsca);
    }

    public String getValue(String columnName) {
        if (columnName.equalsIgnoreCase("tytul")){
            return tytul.getValue();
        } else if (columnName.equalsIgnoreCase("setting")){
            return setting.getValue();
        } else if (columnName.equalsIgnoreCase("prowadzacy")){
            return prowadzacy.getValue();
        } else {
            return null;
        }
    }

    public Integer getValueInt(String columnName) {
        if (columnName.equalsIgnoreCase("max_graczy")){
            return max_graczy.getValue();
        } else if (columnName.equalsIgnoreCase("wolne_miejsca")){
            return wolne_miejsca.getValue();
        } else {
            return null;
        }
    }
}
