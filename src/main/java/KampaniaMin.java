import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KampaniaMin {
    private IntegerProperty id;
    private StringProperty tytul;
    private StringProperty setting;
    private StringProperty prowadzacy;
    private IntegerProperty liczba_graczy;
    private IntegerProperty max_graczy;

    public KampaniaMin() {
        id = new SimpleIntegerProperty();
        tytul = new SimpleStringProperty();
        setting = new SimpleStringProperty();
        prowadzacy = new SimpleStringProperty();
        liczba_graczy = new SimpleIntegerProperty();
        max_graczy = new SimpleIntegerProperty();
    }

    public KampaniaMin(StringProperty tytul, StringProperty setting, IntegerProperty liczba_graczy, IntegerProperty max_graczy) {
        this.tytul = tytul;
        this.setting = setting;
        this.liczba_graczy = liczba_graczy;
        this.max_graczy = max_graczy;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getTytul() {
        return tytul.get();
    }

    public StringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getSetting() {
        return setting.get();
    }

    public StringProperty settingProperty() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting.set(setting);
    }

    public int getLiczba_graczy() {
        return liczba_graczy.get();
    }

    public IntegerProperty liczba_graczyProperty() {
        return liczba_graczy;
    }

    public void setLiczba_graczy(int liczba_graczy) {
        this.liczba_graczy.set(liczba_graczy);
    }

    public int getMax_graczy() {
        return max_graczy.get();
    }

    public IntegerProperty max_graczyProperty() {
        return max_graczy;
    }

    public void setMax_graczy(int max_graczy) {
        this.max_graczy.set(max_graczy);
    }

    public String getProwadzacy() {
        return prowadzacy.get();
    }

    public StringProperty prowadzacyProperty() {
        return prowadzacy;
    }

    public void setProwadzacy(String prowadzacy) {
        this.prowadzacy.set(prowadzacy);
    }

    public String getValue(String columnName) {
        if (columnName.equalsIgnoreCase("tytul")){
            return tytul.getValue();
        } else if (columnName.equalsIgnoreCase("setting")){
            return setting.getValue();
        } else if(columnName.equalsIgnoreCase("max_graczy")){
            return max_graczy.getValue().toString();
        } else if(columnName.equalsIgnoreCase("liczba_graczy")){
            return liczba_graczy.getValue().toString();
        } else if(columnName.equalsIgnoreCase("prowadzacy")) {
            return prowadzacy.getValue();
        }else {
            return null;
        }
    }

    public Integer getValueInt(String columnName) {
        if (columnName.equalsIgnoreCase("liczba_graczy")){
            return liczba_graczy.getValue();
        } else if (columnName.equalsIgnoreCase("max_graczy")){
            return max_graczy.getValue();
        } else {
            return null;
        }
    }
}
