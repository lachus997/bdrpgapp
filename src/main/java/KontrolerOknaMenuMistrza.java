import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna pozwalajacego na wybor miedzy pozadana funkcja dostepna dla banku krwi
 */
public class KontrolerOknaMenuMistrza {

    @FXML
    private Button addButton;

    @FXML
    private Button menageButton;

    @FXML
    private Button wyloguj;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * @throws IOException Wywolanie okna sluzacego do trasnsfuzji wraz z przekazaniem stringa URL do polaczenia z baza danych
     */
    @FXML
    void addKampania(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoNowejKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaNowejKampanii controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) addButton.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @param event
     * @throws IOException Wywolanie okna sluzacego do trasnsfuzji wraz z przekazaniem stringa URL do polaczenia z baza danych oraz z wywolanie metody sluzacej do wyswietlenia zawartosci widoku zapasy w tabeli
     */
    @FXML
    void menageKampanias(ActionEvent event) throws IOException, SQLException {
        System.out.println(connectionUrl);
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoZarzKampaniami.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaZarzKampaniami controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) menageButton.getScene().getWindow();
        stageteraz.close();
    }

//    /**
//     * @param event
//     * @throws IOException
//     * Wywolanie okna sluzacego do zadania parametrow: grupy krwi i czynnika rh wraz z przekazaniem stringa URL do polaczenia z baza danych
//     */
//    @FXML
//    void listapogru(ActionEvent event) throws IOException {
//        FXMLLoader loader = new FXMLLoader();
//        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoWyboruGrupIRh.fxml");
//        loader.setLocation(fxmlUrl);
//        loader.load();
//        KontrolerOknaWyboruGrupIRh controler = loader.getController();
//        controler.setConnectionUrl(connectionUrl);
//        Parent root = loader.getRoot();
//        Scene scene = new Scene(root);
//        Stage primaryStage = new Stage();
//        primaryStage.setScene(scene);
//        primaryStage.setTitle("ProjektBDLog");
//        primaryStage.show();
//
//        Stage stageteraz = (Stage) ZesGrupRhButton.getScene().getWindow();
//        stageteraz.close();
//    }

    /**
     * @param event
     * @throws IOException Metoda cofajaca do poprzediego okna.
     *                     Nie podajke parametru URL wiec nie ma juz danych na bazie ktorych mozliwe byloby ponowne zalogowanie- konieczne jest ponowne wpisanie prawidlowych danych uzytkownka
     */
    @FXML
    private void wyloguj(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoLogowaniaMistrza.fxml");
        loader.setLocation(fxmlUrl);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDStart");
        primaryStage.show();

        Stage stage = (Stage) wyloguj.getScene().getWindow();
        stage.close();
    }

    public void initialize() {
    }
}
