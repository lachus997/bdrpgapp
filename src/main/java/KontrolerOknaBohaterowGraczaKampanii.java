import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna sluzacego do wyswietlania widoku zapasy w przygotowanej tabeli
 */
public class KontrolerOknaBohaterowGraczaKampanii {

    @FXML
    private Button Wroc;

    @FXML
    private TableView<ProstaPostac> tabelka;

    private ObservableList<ProstaPostac> prostaPostacs;

    private String login;

    private int id;

    private int idGracza;

    public int getIdGracza() {
        return idGracza;
    }

    public void setIdGracza(int idGracza) {
        this.idGracza = idGracza;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }



    /**
     * @param event
     * @throws IOException Ponowne otworzenie okna wyboru funkcji i zamkniecie otwartego okna
     */
    @FXML
    void Powrot(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoGraczyKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KotrolerOknaGraczyKampanii controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        controler.setId(id);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @throws SQLException Metoda ta pozwala na nawiazanie polaczenia, wywolacnie select all na widoku zapasy i wyswietlenie jego zawartosci w tabeli
     *                      W funkcji tej najpierw wszystkie kolumnt tabeli sa przygotowane na "odbior danych" wszystkie dane zawarte w widoku sa przetwarzane
     *                      tak, aby mozna je bylo zobrazowac w tabeli
     */
    @FXML
    public void wypelnij() throws SQLException {
        String zap = "Select * from prosty_bohater where gracz_id ='" + idGracza + "' ";
        System.out.println(zap);
        prostaPostacs = FXCollections.observableArrayList();

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            Statement statement = conn.prepareStatement(zap);
            ResultSet rs = statement.executeQuery(zap);
            System.out.println();
            System.out.println(zap);
            System.out.println();
            System.out.println();
            System.out.println(zap);
            System.out.println();

            for (int i = 1; i < rs.getMetaData().getColumnCount() - 1; i++) {

                // We are using non property style for making dynamic table

                String columnName = rs.getMetaData().getColumnName(i + 1);
                TableColumn<ProstaPostac, String> col = new TableColumn<>(columnName.substring(0, 1).toUpperCase() + columnName.replace('_', ' ').substring(1));
                col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ProstaPostac, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ProstaPostac, String> param) {
                        ProstaPostac post = param.getValue();
                        String cellData = post.getValue(columnName);
                        return new SimpleStringProperty(cellData);
                    }
                });
                tabelka.getColumns().add(col);
                System.out.println("Column [" + i + "] ");

            }

            TableColumn<ProstaPostac, Boolean> col_action2 = new TableColumn<>("Edycja");
            col_action2.setSortable(false);
            col_action2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ProstaPostac, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ProstaPostac, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_action2.setCellFactory(new Callback<TableColumn<ProstaPostac, Boolean>, TableCell<ProstaPostac, Boolean>>() {
                @Override
                public TableCell<ProstaPostac, Boolean> call(TableColumn<ProstaPostac, Boolean> p) {
                    return new ButtonCell(tabelka);
                }
            });
            tabelka.getColumns().add(col_action2);

            TableColumn<ProstaPostac, Boolean> col_Delete = new TableColumn<>("Usuwanie");
            col_Delete.setSortable(false);
            col_Delete.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ProstaPostac, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ProstaPostac, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_Delete.setCellFactory(new Callback<TableColumn<ProstaPostac, Boolean>, TableCell<ProstaPostac, Boolean>>() {
                @Override
                public TableCell<ProstaPostac, Boolean> call(TableColumn<ProstaPostac, Boolean> p) {
                    return new ButtonDelete(tabelka);
                }
            });
            tabelka.getColumns().add(col_Delete);

            /********************************
             * Data added to ObservableList *
             ********************************/
            while (rs.next()) {
                ProstaPostac prostaPostac = new ProstaPostac();

                prostaPostac.setId(rs.getInt("id"));
                prostaPostac.setImie(rs.getString("imie"));
                prostaPostac.setNazwisko_pseudonim(rs.getString("nazwisko_pseudonim"));
                prostaPostac.setRasa(rs.getString("rasa"));
                prostaPostac.setKlasa(rs.getString("klasa"));
                prostaPostac.setPoziom(rs.getInt("poziom"));
                prostaPostacs.add(prostaPostac);
            }

            tabelka.setItems(prostaPostacs);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    private void editPostac(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoEdycjiPostacii2.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaEdycjiPostacii2 controler = loader.getController();
        controler.setLogin(login);
        controler.setIdBohatera(id);
        controler.setId(this.id);
        controler.setIdGracza(idGracza);
        controler.setConnectionUrl(connectionUrl);
        controler.start();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    private void usunPosytac(int id) throws SQLException {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement = conn.prepareStatement("delete from bohater where id = ?");
            statement.setInt(1, id);
            int result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        conn.close();
    }

    // Define the button cell
    private class ButtonCell extends TableCell<ProstaPostac, Boolean> {
        final Button cellButton = new Button("Edytuj");

        ButtonCell(final TableView<ProstaPostac> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        editPostac(prostaPostacs.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    private class ButtonDelete extends TableCell<ProstaPostac, Boolean> {
        final Button delButton = new Button("Usun");

        ButtonDelete(final TableView<ProstaPostac> tblView) {
            delButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        usunPosytac(prostaPostacs.get(getIndex()).getId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    prostaPostacs.remove(getIndex());
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(delButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
}