import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna sluzacego do wyswietlania widoku zapasy w przygotowanej tabeli
 */
public class KotrolerOknaGraczyKampanii {

    @FXML
    private Button Wroc;

    @FXML
    private TableView<GraczMin> tabelka;

    private ObservableList<GraczMin> graczMins;

    private String login;

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * @throws IOException Ponowne otworzenie okna wyboru funkcji i zamkniecie otwartego okna
     */
    @FXML
    void Powrot(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoZarzKampaniami.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaZarzKampaniami controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @throws SQLException Metoda ta pozwala na nawiazanie polaczenia, wywolacnie select all na widoku zapasy i wyswietlenie jego zawartosci w tabeli
     *                      W funkcji tej najpierw wszystkie kolumnt tabeli sa przygotowane na "odbior danych" wszystkie dane zawarte w widoku sa przetwarzane
     *                      tak, aby mozna je bylo zobrazowac w tabeli
     */
    @FXML
    public void wypelnij() throws SQLException {
        String zap = "SELECT id, login FROM bdrpg.gracz where id = (select gracz_id from gracz_kampania where kampania_id = ? )";
        System.out.println(zap);
        graczMins = FXCollections.observableArrayList();

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            PreparedStatement statement = conn.prepareStatement(zap);
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();
            System.out.println();
            System.out.println(zap);
            System.out.println();
            System.out.println();
            System.out.println(zap);
            System.out.println();

            for (int i = 1; i < rs.getMetaData().getColumnCount(); i++) {

                    String columnName = rs.getMetaData().getColumnName(i + 1);
                    TableColumn<GraczMin, String> col = new TableColumn<>(columnName);
                    col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GraczMin, String>, ObservableValue<String>>() {
                        public ObservableValue<String> call(TableColumn.CellDataFeatures<GraczMin, String> param) {
                            GraczMin gr = param.getValue();
                            String cellData = gr.getLogin();
                            return new SimpleStringProperty(cellData);
                        }
                    });
                    tabelka.getColumns().add(col);
                    System.out.println("Column [" + i + "] ");

            }

            TableColumn<GraczMin, Boolean> col_action = new TableColumn<>("Lista bohaterow");

            col_action.setSortable(false);
            col_action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GraczMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<GraczMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_action.setCellFactory(new Callback<TableColumn<GraczMin, Boolean>, TableCell<GraczMin, Boolean>>() {
                @Override
                public TableCell<GraczMin, Boolean> call(TableColumn<GraczMin, Boolean> p) {
                    return new ButtonCell2(tabelka);
                }
            });
            tabelka.getColumns().add(col_action);
//
            TableColumn<GraczMin, Boolean> col_Delete = new TableColumn<>("Usuwanie");
            col_Delete.setSortable(false);
            col_Delete.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GraczMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<GraczMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_Delete.setCellFactory(new Callback<TableColumn<GraczMin, Boolean>, TableCell<GraczMin, Boolean>>() {
                @Override
                public TableCell<GraczMin, Boolean> call(TableColumn<GraczMin, Boolean> p) {
                    return new ButtonDelete(tabelka);
                }
            });
            tabelka.getColumns().add(col_Delete);



            /********************************
             * Data added to ObservableList *
             ********************************/
            while (rs.next()) {
                GraczMin graczMin = new GraczMin();

                graczMin.setId(rs.getInt("id"));
                graczMin.setLogin(rs.getString("login"));
                graczMins.add(graczMin);
//                    // Iterate Row
//                    for(int i = 1; i <= rs.getMetaData().getColumnCount(); i++){
//                        // Iterate Column
//                        String columnName = rs.getMetaData().getColumnName(i);
//                        String columnData = rs.getString(i);
//                        book.setValue(columnName, columnData);
//                    }
//                    System.out.println("Row [1] added " + book.getName());
//                    bookData.add(book);

            }
            // FINALLY ADDED TO TableView
            tabelka.setItems(graczMins);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }


    private void listujBohaterow(int id2) throws SQLException, IOException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoBohaterowGraczaKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaBohaterowGraczaKampanii controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        controler.setId(id);
        controler.setIdGracza(id2);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private void editKampania(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoEdycjiKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaEdycjiKampanii controler = loader.getController();
        controler.setLogin(login);
        controler.setid(id);
        controler.setConnectionUrl(connectionUrl);
        controler.Start();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private void usunGracza(int id2) throws SQLException {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement = conn.prepareStatement("delete from gracz_kampania where gracz_id = ? and kampania_id =?");
            statement.setInt(2, id);
            statement.setInt(1, id2);
            int result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        conn.close();
    }

    // Define the button cell
//    private class ButtonCell extends TableCell<GraczMin, Boolean> {
//        final Button cellButton = new Button("Edytuj");
//
//        ButtonCell(final TableView<GraczMin> tblView) {
//            cellButton.setOnAction(new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent t) {
//                    try {
//                        editKampania(graczMins.get(getIndex()).getId());
//                    } catch (IOException | SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
//
//        // Display button if the row is not empty
//        @Override
//        protected void updateItem(Boolean t, boolean empty) {
//            super.updateItem(t, empty);
//            if (!empty) {
//                setGraphic(cellButton);
//            } else {
//                setGraphic(null);
//                setText("");
//            }
//        }
//    }

    private class ButtonCell2 extends TableCell<GraczMin, Boolean> {
        final Button cellButton = new Button("Edytuj");

        ButtonCell2(final TableView<GraczMin> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        listujBohaterow(graczMins.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    private class ButtonDelete extends TableCell<GraczMin, Boolean> {
        final Button delButton = new Button("Usun");

        ButtonDelete(final TableView<GraczMin> tblView) {
            delButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        usunGracza(graczMins.get(getIndex()).getId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    graczMins.remove(getIndex());
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(delButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
}
