import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Klasa kontrolujaca okno logowania dla banku krwi, pozwala ona na przejscie do dalszych funkcji programu
 */
public class KontrolerOknaLogowaniaMistrza {

    @FXML
    private TextField logintext;

    @FXML
    private TextField haslotext;

    @FXML
    private Button logbutton;

    @FXML
    private Button wroc;

    /**
     * @param event
     * @throws IOException Metoda ta pozwala na przejscie do nastepnego okna pozwalajacego na wybor funkcji ktorej chcemy uzyc
     *                     Jednak zanim to nastapi sprawdza czy podane przez uzytkownika login i haslo pozwalaja na polaczenie sie z baza
     *                     jesli tak sie stanie polaczenie zostaje zamkniete, ale kolejne okno zostaje otwarte, a String, ktory pozwolil na polaczenie zostaje przekazany do jego kontrolera jako parametr
     */
    @FXML
    void zaloguj(ActionEvent event) throws IOException {

        String login = logintext.getText();
        String haslo = haslotext.getText();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder urlSB = new StringBuilder("jdbc:mysql://");
        urlSB.append("localhost:3306/");
        urlSB.append("bdrpg?");
        urlSB.append("useUnicode=true&characterEncoding=utf");
        urlSB.append("-8&user=" + login);
        urlSB.append("&password=" + haslo);
        urlSB.append("&serverTimezone=CET");
        String connectionUrl = urlSB.toString();

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            if (conn != null) {
                FXMLLoader loader = new FXMLLoader();
                URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuMistrza.fxml");
                loader.setLocation(fxmlUrl);

                loader.load();
                KontrolerOknaMenuMistrza controler = loader.getController();

                controler.setConnectionUrl(connectionUrl);
                controler.setLogin(login);
                Parent root = loader.getRoot();
                Scene scene = new Scene(root);
                Stage primaryStage = new Stage();
                primaryStage.setScene(scene);
                primaryStage.setTitle("ProjektBDLog");
                primaryStage.show();

                Stage stageteraz = (Stage) logbutton.getScene().getWindow();
                stageteraz.close();
            }
            conn.close();
        } catch (SQLException e) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Problem");
            errorAlert.setContentText("Nieprawidlowy login lub haslo");
            errorAlert.showAndWait();
        }
    }

    /**
     * @param event
     * @throws IOException Metoda sluzaca do ponownego zaladowania poprzedniego okna
     */
    @FXML
    private void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoWyborkuApki.fxml");
        loader.setLocation(fxmlUrl);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDStart");
        primaryStage.show();
        Stage stage = (Stage) wroc.getScene().getWindow();
        stage.close();
    }
}
